FROM opensuse/tumbleweed

RUN zypper -n ref
RUN zypper -n in curl autoconf automake
RUN zypper -n in -t pattern devel_basis
RUN zypper -n in python3-devel
RUN zypper -n in python3-pip
RUN curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly -y
RUN zypper -n in vim

ENV PATH=/root/.cargo/bin:$PATH

VOLUME ["/src"]

ENTRYPOINT [ "/bin/bash" ]
