import random
import string
import templ

def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def gogogo():
    for i in range(200):
        templ.generate(randomString(), "./dump/templ{}".format(i))


if __name__ == "__main__":
    gogogo()
