import random
import string
from Cheetah.Template import Template

with open("template.che", "r") as fd:
    template = fd.readlines()


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def gogogo():
    for i in range(200):
        content = {'name': randomString()}
        t = Template("".join(template), searchList=[content])
        with open("./dump/chee{}".format(i), "w") as fd_out:
            fd_out.write(str(t))

if __name__ == "__main__":
   gogogo()
