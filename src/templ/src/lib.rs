extern crate handlebars;
#[macro_use]
extern crate serde_json;

use std::fs::File;
use std::io::prelude::*;
use pyo3::prelude::*;
use pyo3::wrap_pyfunction;
use handlebars::Handlebars;
use std::path::Path;

/// Formats the sum of two numbers as string.
#[pyfunction]
fn generate(name: String, out_file: String) -> PyResult<String> {
    // read template file
    let mut file = File::open("template.hb")?;
    let mut template = String::new();
    file.read_to_string(&mut template)?;

    let mut reg = Handlebars::new();
    // register template using given name
    reg.register_template_string("tpl_1", template.to_string()).unwrap();
    let ret =  reg.render("tpl_1", &json!({"name": name})).unwrap();

    // now let's write the file to dump
    write_file(out_file, &ret);
    Ok(ret)
}


fn write_file(file_name: String, content: &String) {
    let path = Path::new(&file_name);
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why),
        Ok(file) => file,
    };

    // Write the `LOREM_IPSUM` string to `file`, returns `io::Result<()>`
    file.write_all(content.as_bytes()).unwrap()
}

/// A Python module implemented in Rust.
#[pymodule]
fn templ(py: Python, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(generate))?;

    Ok(())
}
